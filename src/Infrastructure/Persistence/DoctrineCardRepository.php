<?php declare(strict_types=1);

namespace RankUp\Catalog\Infrastructure\Persistence;

use Doctrine\ORM\EntityRepository;
use Parchex\Core\Domain\Exceptions\EntityNotFound;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\CardId;
use RankUp\Catalog\Domain\CardRepository;

class DoctrineCardRepository extends EntityRepository implements CardRepository
{
    public function add(Card $card): void
    {
        $this->getEntityManager()->persist($card);
    }

    public function get(CardId $cardId): Card
    {
        /** @var Card|null $card */
        $card = $this->find($cardId);
        if ($card === null) {
            throw new EntityNotFound($cardId, Card::class);
        }

        return $card;
    }
}
