<?php declare(strict_types=1);

namespace RankUp\Catalog\Infrastructure\Persistence;

use Doctrine\ORM\EntityRepository;
use Parchex\Core\Domain\Exceptions\EntityNotFound;
use RankUp\Catalog\Domain\Candidate;
use RankUp\Catalog\Domain\CandidateId;
use RankUp\Catalog\Domain\CandidateRepository;

class DoctrineCandidateRepository extends EntityRepository implements CandidateRepository
{
    public function get(CandidateId $candidateId): Candidate
    {
        /** @var Candidate|null $candidate */
        $candidate = $this->find($candidateId);
        if ($candidate === null) {
            throw new EntityNotFound($candidateId, Candidate::class);
        }

        return $candidate;
    }
}
