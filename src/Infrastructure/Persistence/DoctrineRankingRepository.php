<?php declare(strict_types=1);

namespace RankUp\Catalog\Infrastructure\Persistence;

use Doctrine\ORM\EntityRepository;
use Parchex\Core\Domain\Exceptions\EntityNotFound;
use RankUp\Catalog\Domain\Ranking;
use RankUp\Catalog\Domain\RankingId;
use RankUp\Catalog\Domain\RankingRepository;

class DoctrineRankingRepository extends EntityRepository implements RankingRepository
{
    public function get(RankingId $rankingId): Ranking
    {
        /** @var Ranking|null $ranking */
        $ranking = $this->find($rankingId);
        if ($ranking === null) {
            throw new EntityNotFound($rankingId, Ranking::class);
        }

        return $ranking;
    }

    public function add(Ranking $ranking): void
    {
        $this->getEntityManager()->persist($ranking);
    }
}
