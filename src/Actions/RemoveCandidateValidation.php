<?php

namespace RankUp\Catalog\Actions;

use RankUp\Catalog\Actions\Validators\CandidateValidation;
use RankUp\Catalog\Actions\Validators\RankingValidation;
use Respect\Validation\Validatable;
use Respect\Validation\Validator as V;

class RemoveCandidateValidation
{
    public static function validator(): Validatable
    {
        return V::create()
            ->key('ranking_id', RankingValidation::rankingId())
            ->key('candidate_id', CandidateValidation::candidateId());
    }
}
