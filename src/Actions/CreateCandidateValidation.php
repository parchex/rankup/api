<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions;

use RankUp\Catalog\Actions\Validators\CandidateValidation;
use RankUp\Catalog\Actions\Validators\CardValidation;
use RankUp\Catalog\Actions\Validators\RankingValidation;
use Respect\Validation\Validatable;
use Respect\Validation\Validator as V;

class CreateCandidateValidation
{
    public static function validator(): Validatable
    {
        return V::oneOf(
            V::create()
                ->key('ranking_id', RankingValidation::rankingId())
                ->key('title', CandidateValidation::title())
                ->key('presentation', CandidateValidation::presentation()),
            V::create()
                ->key('ranking_id', RankingValidation::rankingId())
                ->key('card_id', CardValidation::cardId())
        );
    }
}
