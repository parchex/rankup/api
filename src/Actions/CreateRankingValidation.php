<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions;

use RankUp\Catalog\Actions\Validators\RankingValidation;
use Respect\Validation\Validatable;
use Respect\Validation\Validator as V;

class CreateRankingValidation
{
    public static function validator(): Validatable
    {
        return V::create()
            ->key('title', RankingValidation::title())
            ->key('description', RankingValidation::description());
    }
}
