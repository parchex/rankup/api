<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions;

use Parchex\Core\Application\Command;
use Parchex\Core\Application\Response\Response;
use RankUp\Catalog\Application\EnrollCandidateInRankingCommand;
use RankUp\Catalog\Application\EnrollCandidateInRankingFromACardCommand;

class CreateCandidateAction
{
    use ActionLauncher;

    public function createCandidateCommand(array $input): Command
    {
        return isset($input['card_id'])
            ? new EnrollCandidateInRankingFromACardCommand(
                $input['ranking_id'],
                $input['card_id']
            )
            : new EnrollCandidateInRankingCommand(
                $input['ranking_id'],
                $input['title'],
                $input['presentation']
            );
    }

    /**
     * @param array<string, mixed> $input
     *
     * {@inheritdoc}
     */
    public function __invoke(array $input): Response
    {
        $this->validate($input);

        return $this->launch($this->createCandidateCommand($input));
    }
}
