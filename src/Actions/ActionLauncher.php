<?php

namespace RankUp\Catalog\Actions;

use League\Tactician\CommandBus;
use Parchex\Core\Application\Command;
use Parchex\Core\Application\Response\Response;
use Respect\Validation\Exceptions\ValidationException;
use Respect\Validation\Validatable;

trait ActionLauncher
{
    /**
     * @var CommandBus
     */
    private $commandBus;
    /**
     * @var Validatable
     */
    private $validator;

    public function __construct(CommandBus $commandBus, Validatable $validator)
    {
        $this->commandBus = $commandBus;
        $this->validator = $validator;
    }

    /**
     * @param array $input
     *
     * @throws ValidationException
     */
    private function validate(array $input): void
    {
        $this->validator->assert($input);
    }

    private function launch(Command $command): Response
    {
        return $this->commandBus->handle($command);
    }
}
