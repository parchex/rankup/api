<?php

namespace RankUp\Catalog\Actions;

use RankUp\Catalog\Actions\Validators\CandidateValidation;
use Respect\Validation\Validatable;
use Respect\Validation\Validator as V;

class ModifyCandidateValidation
{
    public static function validator(): Validatable
    {
        return V::create()
            ->key('candidate_id', CandidateValidation::candidateId())
            ->key('title', CandidateValidation::title())
            ->key('presentation', CandidateValidation::presentation());
    }
}
