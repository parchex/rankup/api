<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions;

use RankUp\Catalog\Actions\Validators\CardValidation;
use Respect\Validation\Validatable;
use Respect\Validation\Validator as V;

class CreateCardValidation
{
    public static function validator(): Validatable
    {
        return V::create()
            ->key('title', CardValidation::title())
            ->key('summary', CardValidation::summary())
            ->key('story', CardValidation::story());
    }
}
