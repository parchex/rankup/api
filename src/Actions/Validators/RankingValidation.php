<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions\Validators;

use Respect\Validation\Validatable;
use Respect\Validation\Validator as V;

class RankingValidation
{
    public static function rankingId(): Validatable
    {
        return V::create()->stringType()->alnum('-')->setName("Ranking Id");
    }

    public static function title(): Validatable
    {
        return V::create()->stringType()->notEmpty()->length(1)->setName("Title");
    }

    public static function description(): Validatable
    {
        return V::create()->stringType()->notEmpty()->length(5)->setName("Description");
    }

    public static function maximumCandidates(): Validatable
    {
        return V::create()->number()->min(2)->setName("Max. candidates");
    }

    public static function candidatesAllowed(int $max): Validatable
    {
        return V::create()->arrayVal()->length(0, $max)->setName("Candidates");
    }
}
