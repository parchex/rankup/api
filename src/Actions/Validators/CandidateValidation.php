<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions\Validators;

use Respect\Validation\Validatable;
use Respect\Validation\Validator as V;

class CandidateValidation
{
    public static function title(): Validatable
    {
        return V::create()->stringType()->notEmpty()->length(1)->setName("Title");
    }

    public static function presentation(): Validatable
    {
        return V::create()->stringType()->notEmpty()->length(5)->setName("Presentation");
    }

    public static function candidateId(): Validatable
    {
        return V::create()->stringType()->alnum('-')->setName("Candidate Id");
    }
}
