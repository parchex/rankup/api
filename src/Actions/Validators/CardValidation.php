<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions\Validators;

use Respect\Validation\Validatable;
use Respect\Validation\Validator as V;

class CardValidation
{
    public static function cardId(): Validatable
    {
        return V::create()->stringType()->alnum('-')->setName("Card Id");
    }

    public static function title(): Validatable
    {
        return V::create()->stringType()->notEmpty()->length(1)->setName("Title");
    }

    public static function summary(): Validatable
    {
        return V::create()->stringType()->notEmpty()->length(3)->setName("Summary");
    }

    public static function story(): Validatable
    {
        return V::create()->stringType()->notEmpty()->length(7)->setName("Story");
    }
}
