<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions;

use Parchex\Core\Application\Response\Response;
use RankUp\Catalog\Application\ModifyCandidateContentCommand;

class ModifyCandidateAction
{
    use ActionLauncher;

    /**
     * @param array<string, mixed> $input
     *
     * {@inheritdoc}
     */
    public function __invoke(array $input): Response
    {
        $this->validate($input);

        return $this->launch(
            new ModifyCandidateContentCommand(
                $input['candidate_id'],
                $input['title'],
                $input['presentation']
            )
        );
    }
}
