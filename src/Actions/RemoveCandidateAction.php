<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions;

use Parchex\Core\Application\Response\Response;
use RankUp\Catalog\Application\UnsubscribeCandidateFromRankingCommand;

class RemoveCandidateAction
{
    use ActionLauncher;

    /**
     * @param array<string, mixed> $input
     *
     * {@inheritdoc}
     */
    public function __invoke(array $input): Response
    {
        $this->validate($input);

        return $this->launch(
            new UnsubscribeCandidateFromRankingCommand(
                $input['ranking_id'],
                $input['candidate_id']
            )
        );
    }
}
