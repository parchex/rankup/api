<?php declare(strict_types=1);

namespace RankUp\Catalog\Actions;

use Parchex\Core\Application\Response\Response;
use RankUp\Catalog\Application\CreateCardCommand;

class CreateCardAction
{
    use ActionLauncher;

    /**
     * @param array<string, mixed> $input
     *
     * {@inheritdoc}
     */
    public function __invoke(array $input): Response
    {
        $this->validate($input);

        return $this->launch(
            new CreateCardCommand(
                $input['title'],
                $input['summary'],
                $input['story']
            )
        );
    }
}
