# Changelog

## 0.1.4 - 2023-08-20

* migrate new tool to make project

## v0.1.3 - 2021-10-14

* Fix candidates features

## v0.1.2 - 2020-12-01

* Initial release
* API for manage Rankings
