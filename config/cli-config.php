<?php

/** @var \Psr\Container\ContainerInterface $container */
$container = require dirname(__DIR__) . '/app/bootstrap.php';

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($container->get('entity_manager'));
