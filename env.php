<?php

// Hack to execute behat on docker in PHPStorm
chdir(__DIR__);

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->safeLoad();
