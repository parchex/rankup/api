Feature: Modify content info about a Candidate
  In order to modify information about a Candidate
  As Top Ranker
  I want to update the data of a candidate

  Background:
    Given the "Accept" request header is "application/json"
    And the "Content-Type" request header is "application/json"

  Scenario Outline: Modify a Candidate
    Given the ranking "ranking-id-two" with the following candidates:
      | candidateId        | title           | presentation |
      | candidate-id-one   | The A-Team      | ............ |
      | candidate-id-two   | Blue Thunder    | ............ |
      | candidate-id-three | Stranger Things | ............ |
    And the request body is:
    """
    {
      "title": <title>,
      "presentation": <presentation>
    }
    """
    When I request "rankings/ranking-id-two/candidates/candidate-id-three" using HTTP PUT
    Then the response code is 204
#    And I request "/calendars/ranking-id-ome" using HTTP GET
#    Then the response body contains JSON:
#    """
#    {
#      "ranking_id": "ranking-id-ome",
#      "title": <title>,
#      "description": <presentation>
#    }
#    """
    Examples:
      | title                   | presentation                       |
      | "A title for candidate" | "A presentation for the candidate" |
