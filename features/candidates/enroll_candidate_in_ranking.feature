Feature: Enroll a Candidate in Ranking
  In order to have a ranking with candidates to choose the best
  As a Top Ranker
  I want to enroll candidates in ranking

  Background:
    Given the "Accept" request header is "application/json"
    And the "Content-Type" request header is "application/json"

  Scenario Outline: Enroll Candidate in Ranking
    Given the following rankings with:
      | rankingId        | title            | maximumCandidates | candidates         |
      | ranking-id-one   | The best of all  | 10                | candidate-id-one   |
      | ranking-id-two   | The worst of all | 10                | candidate-id-two   |
      | ranking-id-three | The best friend  | 10                | candidate-id-three |
    And the request body is:
    """
    {
      "title": <title>,
      "presentation": <presentation>
    }
    """
    When I request "rankings/ranking-id-two/candidates" using HTTP POST
    Then the response code is 201
    And the response body contains JSON:
    """
    {
      "candidate_id": "@uuid4()",
      "card_id": "@uuid4()",
      "title": <title>,
      "presentation": <presentation>
    }
    """
    And the response body contains links:
    """
     {
      "_links": {
        "self": {"href":"{base_uri}/rankings/ranking-id-two/candidates/{uuid4}"}
      }
    }
    """
    Examples:
      | title                   | presentation                       |
      | "A title for candidate" | "A presentation for the candidate" |


  Scenario: Enroll Candidate from a Card in Ranking
    Given the following rankings with:
      | rankingId        | title            | maximumCandidates | candidates         |
      | ranking-id-one   | The best of all  | 10                | candidate-id-one   |
      | ranking-id-two   | The worst of all | 10                | candidate-id-two   |
      | ranking-id-three | The best friend  | 10                | candidate-id-three |
    And the following cards with:
      | cardId        | title             | summary          | story                              |
      | card-id-one   | Once upon a time  | The best of all  | A card to know who is the best     |
      | card-id-two   | In the other side | The worst of all | Who knows who is the worst         |
      | card-id-three | Almost one time   | The best friend  | Dos is the best friend,, isn't it? |
    And the request body is:
    """
    {
      "card_id": "card-id-two"
    }
    """
    When I request "rankings/ranking-id-two/candidates" using HTTP POST
    Then the response code is 201
    And the response body contains JSON:
    """
    {
      "candidate_id": "@uuid4()",
      "card_id": "card-id-two",
      "title": "In the other side",
      "presentation": "The worst of all"
    }
    """
    And the response body contains links:
    """
     {
      "_links": {
        "self": {"href":"{base_uri}/rankings/ranking-id-two/candidates/{uuid4}"}
      }
    }
    """
