Feature: Unsubscribe a Candidate from Ranking
  In order to eliminate a candidate for won't be choose the best
  As a Top Ranker
  I want to unsubscribe a candidate from ranking

  Background:
    Given the "Accept" request header is "application/json"
    And the "Content-Type" request header is "application/json"

  Scenario: Unsubscribe Candidate from Ranking
    Given the ranking "ranking-id-one" with the following candidates:
      | candidateId        | title           |
      | candidate-id-one   | The A-Team      |
      | candidate-id-two   | Blue Thunder    |
      | candidate-id-three | Stranger Things |
    And the ranking "ranking-id-two" with the following candidates:
      | candidateId       | title  |
      | candidate-id-four | Blue   |
      | candidate-id-five | Orange |
      | candidate-id-six  | Purple |
    When I request "rankings/ranking-id-two/candidates/candidate-id-five" using HTTP DELETE
    Then the response code is 204
#    And I request "rankings/ranking-id-two/candidates/candidate-id-five"
#    Then the response code is 404
