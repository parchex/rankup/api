Feature: Create a card
  In order to have a card with extended info To use when make candidates
  As a Top Ranker
  I want to create a card

  Background:
    Given the "Accept" request header is "application/json"
    And the "Content-Type" request header is "application/json"

  Scenario Outline: Create a new card
    Given the request body is:
    """
    {
      "title": <title>,
      "summary": <summary>,
      "story": <story>
    }
    """
    When I request "cards" using HTTP POST
    Then the response code is 201
    And the response body contains JSON:
    """
    {
      "card_id": "@uuid4()"
    }
    """
    And the response body contains links:
    """
     {
      "_links": {
        "self": {"href":"{base_uri}/cards/{uuid4}"}
      }
    }
    """
    Examples:
      | title                 | summary                     | story                                                   |
      | "A title for ranking" | "A description for ranking" | "A complete history with description about card entity" |
