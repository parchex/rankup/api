Feature: Modify Card content
  In order to can change the content about a Card
  As Top Ranker
  I want to modify Card data

  Background:
    Given the "Accept" request header is "application/json"
    And the "Content-Type" request header is "application/json"

  Scenario Outline: Modify a Card
    Given the following cards with:
      | cardId        | title             | summary          | story                              |
      | card-id-one   | Once upon a time  | The best of all  | A card to know who is the best     |
      | card-id-two   | In the other side | The worst of all | Who knows who is the worst         |
      | card-id-three | Almost one time   | The best friend  | Dos is the best friend,, isn't it? |
    And the request body is:
    """
    {
      "title": <title>,
      "summary": <summary>
    }
    """
    When I request "cards/card-id-two" using HTTP PUT
    Then the response code is 204
#    And I request "cards/card-id-two" using HTTP GET
#    Then the response body contains JSON:
#    """
#    {
#      "card_id": "card-id-two",
#      "title": <title>,
#      "summary": <summary>,
#      "summary": "Who knows who is the worst"
#    }
#    """
    Examples:
      | title              | summary              |
      | "A title for card" | "A summary for card" |
