Feature: Create a ranking
  In order to group a set of candidates to be voted for the best
  As a Top Ranker
  I want to create a ranking to describe a top list

  Background:
    Given the "Accept" request header is "application/json"
    And the "Content-Type" request header is "application/json"

  Scenario Outline: Create a new ranking
    Given the request body is:
    """
    {
      "title": <title>,
      "description": <description>
    }
    """
    When I request "rankings" using HTTP POST
    Then the response code is 201
    And the response body contains JSON:
    """
    {
      "ranking_id": "@uuid4()",
      "title": <title>,
      "description": <description>
    }
    """
    And the response body contains links:
    """
     {
      "_links": {
        "self": {"href":"{base_uri}/rankings/{uuid4}"}
      }
    }
    """
    Examples:
      | title                 | description                 |
      | "A title for ranking" | "A description for ranking" |
