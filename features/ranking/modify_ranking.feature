Feature: Modify content info about Ranking
  In order to modify information about a Ranking
  As Top Ranker
  I want to update the data of a ranking

  Background:
    Given the "Accept" request header is "application/json"
    And the "Content-Type" request header is "application/json"

  Scenario Outline: Modify a Ranking
    Given the following rankings with:
      | rankingId        | title            | description                       |
      | ranking-id-one   | The best of all  | A ranking to know who is the best |
      | ranking-id-two   | The worst of all | Who knows who is the worst        |
      | ranking-id-three | The best friend  | Dos is the best friend, isn't it? |
    And the request body is:
    """
    {
      "title": <title>,
      "description": <description>
    }
    """
    When I request "rankings/ranking-id-two" using HTTP PUT
    Then the response code is 204
#    And I request "rankings/<ranking_id>" using HTTP GET
#    Then the response body contains JSON:
#    """
#    {
#      "ranking_id": <ranking_id>
#      "title": <title>,
#      "description": <description>
#    }
#    """
    Examples:
      | title                 | description                 |
      | "A title for ranking" | "A description for ranking" |
