<?php declare(strict_types=1);

namespace Features\RankUp\Behat\Contexts;

use Doctrine\ORM\EntityManagerInterface;
use Parchex\Third\Behat\Context\ApiContext;
use Parchex\Third\Doctrine\Fixtures\Executor;
use Parchex\Third\Doctrine\Fixtures\Storage;

class BaseContext extends ApiContext
{
    /**
     * @var Executor
     */
    protected $executor;
    /**
     * @var Storage
     */
    protected $storage;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->executor = new Executor($manager);
        $this->storage = Storage::create($manager);
    }
}
