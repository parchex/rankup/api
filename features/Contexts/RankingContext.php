<?php declare(strict_types=1);

namespace Features\RankUp\Behat\Contexts;

use Behat\Gherkin\Node\TableNode;
use Fixtures\RankUp\Catalog\CandidateBuilder;
use Fixtures\RankUp\Catalog\CardBuilder;
use Fixtures\RankUp\Catalog\RankingBuilder;

class RankingContext extends BaseContext
{
    /**
     * @BeforeScenario
     */
    public function clearDatabase()
    {
        $this->executor->purge();
    }

    /**
     * @Given /^the following rankings with:$/
     */
    public function theFollowingRankingsWith(TableNode $table)
    {
        $this->storage->bulk(RankingBuilder::create(), $table->getColumnsHash());
    }

    /**
     * @Given /^the following cards with:$/
     */
    public function theFollowingCardsWith(TableNode $table)
    {
        $this->storage->bulk(CardBuilder::create(), $table->getColumnsHash());
    }

    /**
     * @Given the ranking :rankingId with the following candidates:
     */
    public function theFollowingCandidatesInRanking($rankingId, TableNode $table)
    {
        $this->storage->fixture(
            RankingBuilder::create(),
            [
                "rankingId" => $rankingId,
                "candidates" => $this->storage->bulk(
                    CandidateBuilder::create(),
                    $table->getColumnsHash()
                ),
            ]
        );
    }
}
