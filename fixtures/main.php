<?php

use Psr\Container\ContainerInterface as Container;

$container[\Doctrine\ORM\EntityManagerInterface::class] = static function (Container $c) {
    return $c->get('entity_manager');
};

/** \Silly\Application $app */
$app->command(
    'fixtures:purge [--truncate] [--yes]',
    function (
        \Doctrine\ORM\EntityManagerInterface $manager,
        \Symfony\Component\Console\Output\OutputInterface $output,
        $truncate,
        $yes
    ) use ($container) {
        $executor = new \Parchex\Third\Doctrine\Fixtures\Executor($container['entity_manager']);

        if ($yes) {
            $output->writeln('<error> ! Purge database...</error>');
            $executor->purge($truncate);
            $output->writeln('<error> ! All data erased...</error>');
        } else {
            $output->writeln(
                '<info>To confirm purge database erasing all data execute command with  option "--yes"'
            );
        }
    }
)->descriptions("For erase fixtures");

$app->command(
    'fixtures:load [-d|--dir=]*',
    function (
        \Doctrine\ORM\EntityManagerInterface $manager,
        \Symfony\Component\Console\Output\OutputInterface $output,
        $dir = null
    ) use ($container) {
        if (empty($dir)) {
            $paths = new CallbackFilterIterator(
                new FilesystemIterator(__DIR__ . DIRECTORY_SEPARATOR),
                function ($current) {
                    return $current->isDir();
                }
            );
        } else {
            $paths = array_map(
                static function ($path) {
                    return new SplFileInfo($path);
                },
                $dir
            );
        }

        $loader = new \Doctrine\Common\DataFixtures\Loader();
        foreach ($paths as $path) {
            $loader->loadFromDirectory($path->getPathName());
        }

        $executor = new \Parchex\Third\Doctrine\Fixtures\Executor(
            $container['entity_manager'],
            new \Symfony\Component\Console\Logger\ConsoleLogger($output)
        );

        $executor->execute($loader);
    }
)->descriptions("Load all fixtures needed");
