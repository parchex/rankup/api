<?php

namespace Fixtures\RankUp\Catalog;

use Doctrine\ORM\EntityManagerInterface;
use Parchex\Third\Doctrine\Fixtures\Storage;
use Symfony\Component\Console\Output\OutputInterface;

class CardLoader extends Loader
{
    public function __invoke(
        EntityManagerInterface $manager,
        OutputInterface $output,
        $total = 5
    ) {
        $fixtureBuilder = Storage::create($manager);

        $output->write("* Creating Cards ($total) => ");
        $output->write('<info>(¬.¬)-</info> ...');

        while ($total-- > 0) {
            $fixtureBuilder->fixture(CardBuilder::create());
            $output->write('.');
        }

        $output->writeln('... <info>\(^o^)/</info>');
    }
}
