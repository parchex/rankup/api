<?php declare(strict_types=1);

namespace Fixtures\RankUp\Catalog;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Loader implements FixtureInterface
{
    public abstract function __invoke(EntityManagerInterface $manager, OutputInterface $output);

    public function load(ObjectManager $manager)
    {
        call_user_func($this, $manager, new NullOutput());
    }
}
