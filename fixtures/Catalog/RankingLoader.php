<?php

namespace Fixtures\RankUp\Catalog;

use Doctrine\ORM\EntityManagerInterface;
use Parchex\Third\Doctrine\Fixtures\Storage;
use RankUp\Catalog\Domain\Card;
use RankUp\Catalog\Domain\Ranking;
use Symfony\Component\Console\Output\OutputInterface;

class RankingLoader extends Loader
{
    public function __invoke(EntityManagerInterface $manager, OutputInterface $output)
    {
        $storage = Storage::create($manager);

        $output->write("* Creating Cards => ");
        $output->write('<info>(¬.¬)-</info> ...');

        $cards = $this->createCards($storage);

        $output->writeln('... <info>\(^o^)/</info>');

        $output->write("* Creating Candidates => ");
        $output->write('<info>(¬.¬)-</info> ...');

        $candidates = $this->createCandidates($storage, $cards);

        $output->writeln('... <info>\(^o^)/</info>');

        $output->write("* Creating Rankings => ");
        $output->write('<info>(¬.¬)-</info> ...');

        $ranking = $this->createRanking($storage, $candidates);

        $output->writeln('... <info>\(^o^)/</info>');
    }

    private function createCards(Storage $storage): array
    {
        $cardBuilder = CardBuilder::create();

        return $storage->bulk(
            $cardBuilder,
            array_map(
                static function () use ($cardBuilder) {
                    return $cardBuilder->reload()->fixtures();
                },
                range(1, 5)
            )
        );
    }

    private function createCandidates(Storage $storage, array $cards): array
    {
        return $storage->bulk(
            CandidateBuilder::create(),
            array_map(
                static function (Card $card) {
                    return ['cardId' => $card->cardId()];
                },
                $cards
            )
        );
    }

    private function createRanking(Storage $storage, array $candidates): Ranking
    {
        return $storage->fixture(RankingBuilder::create(), ['candidates' => $candidates]);
    }
}
