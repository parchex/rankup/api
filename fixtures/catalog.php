<?php

/** \Silly\Application $app */
$app->command(
    'ranking:rankings',
    new \Fixtures\RankUp\Catalog\RankingLoader()
)->descriptions('Fixtures of rankings');

$app->command(
    'ranking:cards [-t|--total=]',
    new \Fixtures\RankUp\Catalog\CardLoader()
)->descriptions(
    'Fixtures of cards',
    [
        '--total' => 'Total number of Cards will be created',
    ]
);
