<?php

defined('__APP_PATH__') || define('__APP_PATH__', dirname(realpath(__DIR__)));
defined('__DEBUG__') || define('__DEBUG__', filter_var(getenv('APP_DEBUG'), FILTER_VALIDATE_BOOLEAN));

return [
    'settings' => [
        'displayErrorDetails' => __DEBUG__,  // set to false in production
        'addContentLengthHeader' => false,   // Allow the web server to send the content-length header
        'routerCacheFile' => false,          // Set a writable file to cache routes in PROD environment

        'app' => [
            'path' => __APP_PATH__,
            'name' => 'RankUp',
            'version' => 'beta',
        ],

        'logger' => [
            'dev_mode' => __DEBUG__,
            'name' => getenv('APP_NAME') ?: 'app',
            'path' => getenv('LOG_PATH') ?: __APP_PATH__ . '/var/log/app.log',
            'level' => getenv('LOG_LEVEL') ?: Psr\Log\LogLevel::DEBUG,
        ],

        'command_bus' => [
            'dev_mode' => __DEBUG__,
            'middleware' => [
                \League\Tactician\Doctrine\ORM\TransactionMiddleware::class,
                \League\Tactician\Plugins\LockingMiddleware::class,
                \Parchex\Lump\Events\League\PersistAllEventsEmittedTacticianMiddleware::class,
            ],
        ],

        'doctrine' => [
            'dev_mode' => __DEBUG__,
            // path where the compiled metadata info will be cached
            // make sure the path exists and it is writable
            'proxy_dir' => __APP_PATH__ . '/var/cache/proxies',
            'cache_dir' => __APP_PATH__ . '/var/cache/doctrine',
            'connection' => [
                'url' => getenv('RANKUP_API_DB_CONN'),
            ],
        ],

        'api' => [
            'json_encoding_options' => JSON_UNESCAPED_SLASHES | (__DEBUG__ ? JSON_PRETTY_PRINT : 0),
        ],
    ],
];
