<?php

use Psr\Container\ContainerInterface as Container;

// Monolog
$container['logger'] = function (Container $c) {
    return \Parchex\Lump\Log\Logger::instance($c->get('settings')['logger']);
};

$container['logger.doctrine'] = function (Container $c) {
    return $c->get('logger')->withName('doctrine');
};
