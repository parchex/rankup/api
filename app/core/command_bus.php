<?php

use Psr\Container\ContainerInterface as Container;

// settings default values
$container['settings']['command_bus'] = array_replace_recursive(
    [
        'inflector'  => \League\Tactician\Handler\MethodNameInflector\HandleInflector::class,
        // 'locator'    => \League\Tactician\Handler\Locator\InMemoryLocator::class,
        'locator'    => \League\Tactician\Container\ContainerLocator::class,
        'extractor'  => \League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor::class,
        'middleware' => [],
        'mapping'    => [],
    ],
    (array) $container['settings']['command_bus']
);

// service
$container['command_bus'] = function (Container $c) {
    $settings = $c->get('settings')['command_bus'];

    $extractor = new $settings['extractor'];
    $locator = $c->get($settings['locator']);
    $inflector = new $settings['inflector'];

    if ($settings['dev_mode']) {
        $settings['middleware'][] = \League\Tactician\Logger\LoggerMiddleware::class;
    }

    $middleware = array_map(
        function ($mid) use ($c) {
            return $c->has($mid) ? $c->get($mid) : new $mid;
        },
        $settings['middleware']
    );

    $middleware[] = new \League\Tactician\Handler\CommandHandlerMiddleware($extractor, $locator, $inflector);

    return new \League\Tactician\CommandBus($middleware);
};

$container[\League\Tactician\Container\ContainerLocator::class] = function (Container $c) {
    return new \League\Tactician\Container\ContainerLocator(
        $c,
        $c->get('settings.all')['command_bus']['mapping']
    );
};

$container[\League\Tactician\Handler\Locator\InMemoryLocator::class] = function (Container $c) {
    $locator = new \League\Tactician\Handler\Locator\InMemoryLocator();

    foreach ($c->get('settings.all')['command_bus']['mapping'] as $command => $handler) {
        $locator->addHandler($c->has($handler) ? $c->get($handler) : new $handler, $command);
    }

    return $locator;
};

// Middlewares...
$container[\League\Tactician\Doctrine\ORM\TransactionMiddleware::class] = function (Container $c) {
    return new \League\Tactician\Doctrine\ORM\TransactionMiddleware($c->get('entity_manager'));
};

$container[\League\Tactician\Logger\LoggerMiddleware::class] = function (Container $c) {
    return new \League\Tactician\Logger\LoggerMiddleware(
        new \League\Tactician\Logger\Formatter\ClassPropertiesFormatter(),
        $c->get('logger')
    );
};

$container[\Parchex\Lump\Events\League\PersistAllEventsEmittedTacticianMiddleware::class] = function (Container $c) {
    return new \Parchex\Lump\Events\League\PersistAllEventsEmittedTacticianMiddleware(
        $c->get('entity_manager')->getRepository(\Parchex\Lump\Events\Persistence\Event::class),
        \Parchex\Core\Domain\EventManager::subscriber(),
        new \Parchex\Lump\Events\Persistence\AllEventsListener()
    );
};
