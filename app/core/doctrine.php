<?php

use Psr\Container\ContainerInterface as Container;

// settings default values
$container['settings']['doctrine'] = array_replace_recursive(
    [
        'dev_mode'   => false,
        'proxy_dir'  => null,
        // database configuration parameters
        'connection' => [],
        'mapping'    => [],
        'types'      => [
            \Parchex\Common\DateTime::class => \Parchex\Third\Doctrine\Types\DateTimeType::class,
            \Parchex\Core\Domain\Id\Uuid::class,
        ],
    ],
    (array) $container['settings']['doctrine']
);

// Entity Manager
$container['entity_manager'] = static function (Container $c) {
    return \Doctrine\ORM\EntityManager::create(
        $c->get('settings')['doctrine']['connection'],
        $c->get('doctrine.config')
    );
};

$container['doctrine.config'] = static function (Container $c) {
    // Return settings with all modules config added
    $settings = $c->get('settings.all')['doctrine'];

    // Register Doctrine types
    \Parchex\Third\Doctrine\Types\TypeRegister::all($settings['types']);

    $config = \Doctrine\ORM\Tools\Setup::createConfiguration(
        $settings['dev_mode'],
        $settings['proxy_dir'],
        $settings['dev_mode']
            ?
            new \Doctrine\Common\Cache\ArrayCache()
            :
            new \Doctrine\Common\Cache\FilesystemCache($settings['cache_dir'])
    // new Doctrine\Common\Cache\ApcuCache();
    );

    $driver = new \Doctrine\Persistence\Mapping\Driver\MappingDriverChain();
    $driver->setDefaultDriver(new \Doctrine\ORM\Mapping\Driver\SimplifiedXmlDriver($settings['mapping']));
    $driver->addDriver(
        new \Parchex\Third\Doctrine\Events\MappingDriver(),
        \Parchex\Third\Doctrine\Events\MappingDriver::NS_EVENT
    );
    $config->setMetadataDriverImpl($driver);

    $config->setNamingStrategy(
        new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy(CASE_LOWER)
    );

    $config->setProxyNamespace('Parchex\Entity\Proxies');
    $config->setAutoGenerateProxyClasses(
        $settings['dev_mode']
            ? Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_ALWAYS
            : Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_NEVER
    );

    $config->setSQLLogger($c->get(\Doctrine\DBAL\Logging\SQLLogger::class));

    return $config;
};

$container[\Doctrine\DBAL\Logging\SQLLogger::class] = function (Container $c) {
    return new \Parchex\Third\Doctrine\SqlLogger($c->get('logger.doctrine'));
};
