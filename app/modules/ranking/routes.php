<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

// Rankings
$app->post(
    '/rankings',
    function (Request $request, Response $response, $args) {
        $input = (array) $request->getParsedBody();

        $payload = $this->get(\RankUp\Catalog\Actions\CreateRankingAction::class)($input);

        $route = 'ranking';
        $args['ranking_id'] = $payload['ranking_id'];

        return $this
            ->get('api.response.links.item')($payload, $request, $route, $args)($response, 201);
    }
);

$app->put(
    '/rankings/{ranking_id}',
    function (Request $request, Response $response, $args) {
        $input = array_replace((array) $request->getParsedBody(), $args);

        $this->get(\RankUp\Catalog\Actions\ModifyRankingAction::class)($input);

        return $this->get('api.response')($response, 204);
    }
);

$app->get(
    '/rankings/{ranking_id}',
    function (Request $request, Response $response, array $args) {
        $payload = $this->get('ranking.action.get_card')($args);

        return $this
            ->get('api.response.links.item')($payload, $request)($response, 200);
    }
)->setName('ranking');

// Candidates
$app->post(
    '/rankings/{ranking_id}/candidates',
    function (Request $request, Response $response, $args) {
        $input = array_replace((array) $request->getParsedBody(), $args);

        $payload = $this->get(\RankUp\Catalog\Actions\CreateCandidateAction::class)($input);

        $route = 'candidate';
        $args['candidate_id'] = $payload['candidate_id'];

        return $this
            ->get('api.response.links.item')($payload, $request, $route, $args)($response, 201);
    }
);

$app->put(
    '/rankings/{ranking_id}/candidates/{candidate_id}',
    function (Request $request, Response $response, $args) {
        $input = array_replace((array) $request->getParsedBody(), $args);

        $this->get(\RankUp\Catalog\Actions\ModifyCandidateAction::class)($input);

        return $this->get('api.response')($response, 204);
    }
);

$app->delete(
    '/rankings/{ranking_id}/candidates/{candidate_id}',
    function (Request $request, Response $response, array $args) {
        $this->get(\RankUp\Catalog\Actions\RemoveCandidateAction::class)($args);

        return $this->get('api.response')($response, 204);
    }
);

$app->get(
    '/rankings/{ranking_id}/candidates/{candidate_id}',
    function (Request $request, Response $response, array $args) {
        $payload = $this->get('ranking.action.get_candidate')($args);

        return $this
            ->get('api.response.links.item')($payload, $request)($response, 200);
    }
)->setName('candidate');

// Cards
$app->post(
    '/cards',
    function (Request $request, Response $response, $args) {
        $input = (array) $request->getParsedBody();

        $payload = $this->get(\RankUp\Catalog\Actions\CreateCardAction::class)($input);

        $route = 'card';
        $args['card_id'] = $payload['card_id'];

        return $this
            ->get('api.response.links.item')($payload, $request, $route, $args)($response, 201);
    }
);

$app->put(
    '/cards/{card_id}',
    function (Request $request, Response $response, $args) {
        $input = array_replace((array) $request->getParsedBody(), $args);

        $this->get(\RankUp\Catalog\Actions\ModifyCardAction::class)($input);

        return $this->get('api.response')($response, 204);
    }
);

$app->get(
    '/cards/{card_id}',
    function (Request $request, Response $response, array $args) {
        $payload = $this->get('ranking.action.get_card')($args);

        return $this->get('api.response.links.item')($payload, $request)($response, 200);
    }
)->setName('card');
