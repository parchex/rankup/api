<?php

use Psr\Container\ContainerInterface as Container;

// settings default values
$container['settings']['ranking'] = array_merge_recursive(
    [
        'command_bus' => [
            'mapping' => [
                \RankUp\Catalog\Application\CreateCardCommand::class
                => \RankUp\Catalog\Application\CreateCardHandler::class,
                \RankUp\Catalog\Application\ModifyCardContentCommand::class
                => \RankUp\Catalog\Application\ModifyCardContentHandler::class,
                \RankUp\Catalog\Application\CreateRankingCommand::class
                => \RankUp\Catalog\Application\CreateRankingHandler::class,
                \RankUp\Catalog\Application\ModifyRankingContentCommand::class
                => \RankUp\Catalog\Application\ModifyRankingContentHandler::class,
                \RankUp\Catalog\Application\EnrollCandidateInRankingCommand::class
                => \RankUp\Catalog\Application\EnrollCandidateInRankingHandler::class,
                \RankUp\Catalog\Application\EnrollCandidateInRankingFromACardCommand::class
                => \RankUp\Catalog\Application\EnrollCandidateInRankingFromACardHandler::class,
                \RankUp\Catalog\Application\UnsubscribeCandidateFromRankingCommand::class
                => \RankUp\Catalog\Application\UnsubscribeCandidateFromRankingHandler::class,
                \RankUp\Catalog\Application\ModifyCandidateContentCommand::class
                => \RankUp\Catalog\Application\ModifyCandidateContentHandler::class,
            ],
        ],
        'doctrine' => [
            'mapping' => [
                __DIR__ . '/doctrine' => 'RankUp\Catalog\Domain',
            ],
            'types' => [
                \RankUp\Catalog\Domain\RankingId::class,
                \RankUp\Catalog\Domain\CardId::class,
                \RankUp\Catalog\Domain\CandidateId::class,
            ],
        ],
    ],
    (array) $container['settings']['ranking']
);

// Repos
$container[\RankUp\Catalog\Domain\CardRepository::class] = function (Container $c) {
    return $c->get('entity_manager')->getRepository(\RankUp\Catalog\Domain\Card::class);
};

$container[\RankUp\Catalog\Domain\RankingRepository::class] = function (Container $c) {
    return $c->get('entity_manager')->getRepository(\RankUp\Catalog\Domain\Ranking::class);
};

$container[\RankUp\Catalog\Domain\CandidateRepository::class] = function (Container $c) {
    return $c->get('entity_manager')->getRepository(\RankUp\Catalog\Domain\Candidate::class);
};

// Commands
$container[\RankUp\Catalog\Application\CreateCardHandler::class] = function (Container $c) {
    return new \RankUp\Catalog\Application\CreateCardHandler(
        $c->get(\RankUp\Catalog\Domain\CardRepository::class)
    );
};

$container[\RankUp\Catalog\Application\ModifyCardContentHandler::class] = function (Container $c) {
    return new \RankUp\Catalog\Application\ModifyCardContentHandler(
        $c->get(\RankUp\Catalog\Domain\CardRepository::class)
    );
};

$container[\RankUp\Catalog\Application\CreateRankingHandler::class] = function (Container $c) {
    return new \RankUp\Catalog\Application\CreateRankingHandler(
        $c->get(\RankUp\Catalog\Domain\RankingRepository::class)
    );
};

$container[\RankUp\Catalog\Application\ModifyRankingContentHandler::class] =
    function (Container $c) {
        return new \RankUp\Catalog\Application\ModifyRankingContentHandler(
            $c->get(\RankUp\Catalog\Domain\RankingRepository::class)
        );
    };

$container[\RankUp\Catalog\Application\EnrollCandidateInRankingFromACardHandler::class] =
    function (Container $c) {
        return new \RankUp\Catalog\Application\EnrollCandidateInRankingFromACardHandler(
            $c->get(\RankUp\Catalog\Domain\RankingRepository::class),
            $c->get(\RankUp\Catalog\Domain\CardRepository::class)
        );
    };

$container[\RankUp\Catalog\Application\EnrollCandidateInRankingHandler::class] =
    function (Container $c) {
        return new \RankUp\Catalog\Application\EnrollCandidateInRankingHandler(
            $c->get(\RankUp\Catalog\Domain\RankingRepository::class),
            $c->get(\RankUp\Catalog\Domain\CardRepository::class)
        );
    };

$container[\RankUp\Catalog\Application\UnsubscribeCandidateFromRankingHandler::class] =
    function (Container $c) {
        return new \RankUp\Catalog\Application\UnsubscribeCandidateFromRankingHandler(
            $c->get(\RankUp\Catalog\Domain\RankingRepository::class),
            $c->get(\RankUp\Catalog\Domain\CandidateRepository::class)
        );
    };

$container[\RankUp\Catalog\Application\ModifyCandidateContentHandler::class] =
    function (Container $c) {
        return new \RankUp\Catalog\Application\ModifyCandidateContentHandler(
            $c->get(\RankUp\Catalog\Domain\CandidateRepository::class)
        );
    };

// Actions
$container[\RankUp\Catalog\Actions\CreateCardAction::class] = function (Container $c) {
    return new \RankUp\Catalog\Actions\CreateCardAction(
        $c->get('command_bus'),
        \RankUp\Catalog\Actions\CreateCardValidation::validator()
    );
};

$container[\RankUp\Catalog\Actions\ModifyCardAction::class] = function (Container $c) {
    return new \RankUp\Catalog\Actions\ModifyCardAction(
        $c->get('command_bus'),
        \RankUp\Catalog\Actions\ModifyCardValidation::validator()
    );
};

$container[\RankUp\Catalog\Actions\CreateRankingAction::class] = function (Container $c) {
    return new \RankUp\Catalog\Actions\CreateRankingAction(
        $c->get('command_bus'),
        \RankUp\Catalog\Actions\CreateRankingValidation::validator()
    );
};

$container[\RankUp\Catalog\Actions\ModifyRankingAction::class] = function (Container $c) {
    return new \RankUp\Catalog\Actions\ModifyRankingAction(
        $c->get('command_bus'),
        \RankUp\Catalog\Actions\ModifyRankingValidation::validator()
    );
};

$container[\RankUp\Catalog\Actions\CreateCandidateAction::class] = function (Container $c) {
    return new \RankUp\Catalog\Actions\CreateCandidateAction(
        $c->get('command_bus'),
        \RankUp\Catalog\Actions\CreateCandidateValidation::validator()
    );
};

$container[\RankUp\Catalog\Actions\RemoveCandidateAction::class] = function (Container $c) {
    return new \RankUp\Catalog\Actions\RemoveCandidateAction(
        $c->get('command_bus'),
        \RankUp\Catalog\Actions\RemoveCandidateValidation::validator()
    );
};

$container[\RankUp\Catalog\Actions\ModifyCandidateAction::class] = function (Container $c) {
    return new \RankUp\Catalog\Actions\ModifyCandidateAction(
        $c->get('command_bus'),
        \RankUp\Catalog\Actions\ModifyCandidateValidation::validator()
    );
};
