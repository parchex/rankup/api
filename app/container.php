<?php

// DIC configuration
$container = new \Slim\Container(require __DIR__ . '/settings.php');
//
require __DIR__ . '/core/logger.php';
require __DIR__ . '/core/doctrine.php';
require __DIR__ . '/core/command_bus.php';
// Modules...
require __DIR__ . '/modules/ranking/dependencies.php';

$container['settings.all'] = function (\Slim\Container $c) {
    $settings = $c->get('settings');

    $merge = [
        'doctrine'    => [$settings['doctrine']],
        'command_bus' => [$settings['command_bus']],
    ];

    foreach ($settings as $config) {
        if (isset($config['doctrine'])) {
            $merge['doctrine'][] = (array) $config['doctrine'];
        }
        if (isset($config['command_bus'])) {
            $merge['command_bus'][] = (array) $config['command_bus'];
        }
    }

    return new ArrayObject(
        array_map(
            static function ($configs) {
                return array_merge_recursive(...$configs);
            },
            $merge
        )
    );
};

return $container;
