<?php

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$container[\Respect\Validation\Exceptions\ValidationException::class] = function (Container $c) {
    return function (
        Request $request,
        Response $response,
        \Respect\Validation\Exceptions\ValidationException $exc
    ) use ($c) {
        return $c->get('api.response.error')(
            $response,
            400,
            $exc->getFullMessage(),
            $exc,
            [
                'validations' =>
                    $exc instanceof \Respect\Validation\Exceptions\NestedValidationException
                        ? $exc->getMessages()
                        : $exc->getMessage(),
            ]
        );
    };
};

$container[\LogicException::class] = function (Container $c) {
    return function (
        Request $request,
        Response $response,
        \Exception $exc
    ) use ($c) {
        return $c->get('api.response.error')(
            $response,
            400,
            $exc->getMessage(),
            $exc
        );
    };
};

$container[\Parchex\Core\Domain\Exceptions\EntityNotFound::class] = function (Container $c) {
    return function (
        Request $request,
        Response $response,
        \Exception $exc
    ) use ($c) {
        return $c->get('api.response.error')(
            $response,
            404,
            $exc->getMessage(),
            $exc
        );
    };
};

$container[\Exception::class] = function (Container $c) {
    return function (
        Request $request,
        Response $response,
        \Throwable $exc
    ) use ($c) {
        return $c->get('api.response.error')(
            $response,
            500,
            $exc->getMessage(),
            $exc
        );
    };
};

$container['phpErrorHandler'] = $container->raw(\Exception::class);

$container['errorHandler'] = function (Container $c) {
    return function (
        Request $request,
        Response $response,
        \Exception $exc
    ) use ($c) {
        $typeError = get_class($exc);

        if ($c->has($typeError)) {
            return $c->get($typeError)($request, $response, $exc);
        }

        if ($exc instanceof \LogicException) {
            $typeError = \LogicException::class;
        }
        if ($exc instanceof \Respect\Validation\Exceptions\ValidationException) {
            $typeError = \Respect\Validation\Exceptions\ValidationException::class;
        }

        if (!$c->has($typeError)) {
            $typeError = \Exception::class;
        }

        return $c->get($typeError)($request, $response, $exc);
    };
};
