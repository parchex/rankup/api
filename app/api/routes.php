<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get(
    '/',
    function (Request $request, Response $response) {
        /** @var \Slim\Router $router */
        $router = $this->get('router');

        $api = [];
        foreach ($router->getRoutes() as $route) {
            $api[$route->getPattern()]['path'] = $route->getPattern();

            foreach ($route->getMethods() as $method) {
                $api[$route->getPattern()]['methods'][] = $method;
            }
        }

        return $this->get('api.response')($response, 200, $api);
    }
);
