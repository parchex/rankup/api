<?php

use Parchex\Core\Application\Response\Item;
use Psr\Container\ContainerInterface as Container;
use Slim\Http\Request;

$container['settings']['api'] = array_replace_recursive(
    [
        'json_encoding_options' => JSON_UNESCAPED_SLASHES,
    ],
    (array) $container['settings']['api']
);

$container['api.response'] = static function (Container $c) {
    return static function (
        \Slim\Http\Response $response,
        int $status = 200,
        $payload = null
    ) use ($c) {
        return $response->withJson(
            $payload,
            $status,
            $c->get('settings')['api']['json_encoding_options']
        );
    };
};

$container['api.response.payload'] = static function (Container $c) {
    return static function ($payload) use ($c) {
        return static function (
            \Slim\Http\Response $response,
            int $status = 200
        ) use ($c, $payload) {
            return $c->get('api.response')($response, $status, $payload);
        };
    };
};

$container['api.response.error'] = static function (Container $c) {
    return static function (
        \Slim\Http\Response $response,
        int $status,
        string $message,
        Throwable $exc,
        array $extras = []
    ) use ($c) {
        if ($status >= 500) {
            $c->get('logger')->critical($exc->getMessage(), $exc->getTrace());
        } else {
            $c->get('logger')->error($exc->getMessage(), $exc->getTrace());
        }

        return $c->get('api.response')(
            $response,
            $status,
            array_merge(
                ['type' => get_class($exc), 'message' => $message],
                $extras,
                ['trace' => explode("\n", $exc->getTraceAsString())]
            )
        );
    };
};

$container['api.response.links.item'] = static function (Container $c) {
    return static function (
        Item $payload,
        Request $request,
        string $routeName = null,
        array $routeParams = [],
        string $linkName = 'self'
    ) use ($c) {
        /** @var \Slim\Router $router */
        $router = $c->get('router');

        return $c->get('api.response.payload')(
            $payload->copyWith(
                [
                    '_links' => [
                        $linkName => [
                            'href' => $routeName
                                ? $router->fullUrlFor(
                                    $request->getUri(),
                                    $routeName,
                                    $routeParams
                                )
                                : (string) $request->getUri(),
                        ],
                    ],
                ]
            )
        );
    };
};
