#!/bin/bash

if [[ ! -e .makeUp ]]; then
. <(curl -sSL https://gitlab.com/oxkhar/makeup/raw/master/install.sh)
fi

. .makeUp/aliases-install.sh
. .makeUp/aliases-config.sh

## Config files env
# : ${APP_ENV_CONFIG:=".env"}
# : ${APP_ENV_FILE:=".env.local"}
# : ${DOCKER_ENV_CONFIG:="docker.env"}
# : ${DOCKER_ENV_FILE:=".env.docker"}
aliases-config

## Declare here env variables to configure
# : ${DOCKER_IMAGE_RUN:=${DOCKER_REGISTRY}${APP_PROJECT}/${APP_NAME}:${APP_VERSION}}
# : ${DOCKER_BUILD_FILE:=Dockerfile}
# : ${DOCKER_COMPOSE_FILE:=docker-compose.yaml}
# : ${DOCKER_SERVICE_RUN:=cli}

## Choose one...
# : ${PHP_CLI:="php"}
# : ${PHP_CLI:="php-docker"} # Default
: ${PHP_CLI:="php-docker-compose"}
## For debugging...
# : ${PHP_DEBUG:="php-xdebug"} # Default
: ${PHP_DEBUG:="php-xdebug2"}

##
. .makeUp/aliases-docker.sh
. .makeUp/aliases-php.sh
. .makeUp/aliases-server.sh
# . .makeUp/aliases-k8s.sh
# . .makeUp/aliases-js.sh
