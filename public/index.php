<?php

$app_path = dirname(realpath(__DIR__)) . '/app';

$container = require $app_path . '/bootstrap.php';
// Include api services...
require $app_path . '/api/errors.php';
require $app_path . '/api/response.php';

$app = new \Slim\App($container);

// Register middleware
require $app_path . '/api/middleware.php';

// Register routes
require $app_path . '/routes.php';

// Run app
$app->run();
