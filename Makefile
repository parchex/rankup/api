### Components config selection
## More info on https://gitlab.com/oxkhar/makeup/blob/master/example/Makefile
##
# Docker (choose one... )
#  values: docker-compose | docker-engine (default)
MAKEUP_HAS += docker-compose
##
# Merge release (choose one... )
#  values:  git-flow | git-trunk (default)
MAKEUP_HAS += git-trunk
##
# PHP options...
#  values: php-composer php-build
#          php-qa
#          php-doctrine php-symfony
#          php-tests-xunit php-tests-mutant php-tests-rspec php-tests-bdd
MAKEUP_HAS += php-composer php-build php-doctrine php-tests-bdd php-qa
### Environment configuration...
##
# PHP_CLI = ${PHP_CLI_LOCAL} # For PHP local installation
DATA_DIRS := mysql sqlite
PHPMETRICS_OPTIONS := --exclude=var,vendor,bin,build

### Tag rules to update files with new version
TAG_APP_FILES += ./.env app/settings.php docs/openapi.yaml
./.env.tag: TAG_REG_EXP=/APP_VERSION=.*/APP_VERSION=${TAG}/
app/settings.php.tag: TAG_REG_EXP=/version[ ]*=>.*/version => '${TAG}';/
docs/openapi.yaml.tag: TAG_REG_EXP=/version:.*/version: '${TAG}'/

###
MKUP_INSTALL_SETUP = https://gitlab.com/oxkhar/makeup/raw/master/install.sh
MKUP_INSTALL_PATH  = .makeUp/MakeUp.mk
$(shell [ ! -e "${MKUP_INSTALL_PATH}" ] && curl -sSL "${MKUP_INSTALL_SETUP}" | bash)

include ${MKUP_INSTALL_PATH}
